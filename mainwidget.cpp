#include "mainwidget.h"
#include "ui_mainwidget.h"

#include "primaryflightdisplay.h"

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget),
    settingsDialog(this)
{
    ui->setupUi(this);

    ds = new DataSource(this);
    ds->registerDataSource("n.heading");
    ds->registerDataSource("n.pitch");
    ds->registerDataSource("n.roll");

    connect(ds,&DataSource::flightDataUpdated,this,&MainWidget::updateFlightData);
    connect(ds,&DataSource::stateChanged,this,&MainWidget::onDataSourceStateChanged);
    connect(this,&MainWidget::widgetLoaded,&settingsDialog,&SettingsDialog::exec,Qt::ConnectionType(Qt::QueuedConnection | Qt::UniqueConnection));
    connect(&settingsDialog,&SettingsDialog::newSettingsAccepted,this,&MainWidget::onNewSettingsAvailable);
}

void MainWidget::updateFlightData(QVariantMap* data){
    ui->pfd->updateFlightData(data);
}

void MainWidget::onNewSettingsAvailable(SettingsDialog::Settings newSettings){
    ds->close();
    ds->setServer(newSettings.host,newSettings.port);
    ds->setDataRate(newSettings.refreshRate);
    ds->open();
}

void MainWidget::onDataSourceStateChanged(DataSource::State state){
    switch(state){
    case DataSource::NoConnectionState:
        qDebug()<<"State : Offline";
        break;
    case DataSource::ConnectingState:
        qDebug()<<"State : Connecting";
        break;
    case DataSource::ConnectedState:
        qDebug()<<"State : No Data";
        break;
    case DataSource::RunningState:
        qDebug()<<"State : Running";
        break;
    }
}


void MainWidget::showEvent(QShowEvent *event){
    QWidget::showEvent(event);
    static int cpt = 0;
    if(cpt == 0 ){
        emit widgetLoaded();
        cpt = 1;
    }
}

MainWidget::~MainWidget()
{
    delete ui;
}
