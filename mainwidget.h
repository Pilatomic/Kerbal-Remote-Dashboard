#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include "settingsdialog.h"
#include "datasource.h"

#include <QWidget>
#include <QMap>
#include <QVariant>

namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();


public slots:
    void updateFlightData(QVariantMap *data);

private slots:
    void onNewSettingsAvailable(SettingsDialog::Settings newSettings);
    void onDataSourceStateChanged(DataSource::State state);

signals:
    void widgetLoaded();

protected:
    void showEvent(QShowEvent* event);

private:
    Ui::MainWidget *ui;
    SettingsDialog settingsDialog;
    DataSource* ds;
};

#endif // MAINWIDGET_H
