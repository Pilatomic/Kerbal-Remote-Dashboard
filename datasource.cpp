#include "datasource.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QTime>


DataSource::DataSource(QObject *parent) :
    QObject(parent),
    dataRate(50)
{
    url.setScheme(QStringLiteral("ws"));
    url.setPath(QStringLiteral("/datalink"));

    connect(&webSocket, &QWebSocket::stateChanged, this, &DataSource::onSocketStateChanged);
    connect(&webSocket, &QWebSocket::textMessageReceived,this, &DataSource::onTextMessageReceived);
}

void DataSource::open(){
    webSocket.open(url);
}

void DataSource::close(){
    webSocket.close(QWebSocketProtocol::CloseCodeNormal);
}

void DataSource::setServer(QString server, int port){
    url.setHost(server);
    url.setPort(port);
}

void DataSource::setDataRate(int newRate){
    if(newRate<=0) return;
    dataRate = newRate;
    if(webSocket.isValid())
        sendRateMessage();
}

void DataSource::sendRateMessage(){
    webSocket.sendTextMessage(QString("{\"rate\": %1}").arg(dataRate));
}

void DataSource::registerDataSources(QStringList sourceList){
    if(sourceList.isEmpty())return;
    registeredData.append(sourceList);
    if(webSocket.isValid())
        sendRegisterMessage(sourceList);
}

void DataSource::registerDataSource(QString source){
    if(source.isEmpty())return;
    registeredData.append(source);
    if(webSocket.isValid())
        sendRegisterMessage(QStringList(source));
}

void DataSource::sendRegisterMessage(QStringList sourceList){
    QString message("{\"+\":[\"%1]\"}");
    message = message.arg(sourceList.join("\", \""));
    webSocket.sendTextMessage(message);
    //qDebug()<<"Register message sent : "<<message;
}


void DataSource::onSocketStateChanged(QAbstractSocket::SocketState state){
    switch(state){
    case QAbstractSocket::ConnectingState:
        flightData.clear();
        emit stateChanged(ConnectingState);
        break;
    case QAbstractSocket::ConnectedState:
        sendRateMessage();
        sendRegisterMessage(registeredData);
        emit stateChanged(ConnectedState);
        break;
    case QAbstractSocket::UnconnectedState:
        flightData.clear();
        emit stateChanged(NoConnectionState);
        break;
    default:
        break;
    }
}

void DataSource::onTextMessageReceived(QString message){
    QJsonDocument myDocument = QJsonDocument::fromJson(message.toUtf8());
    if(myDocument.isObject()){
        QVariantMap newFlightData = myDocument.object().toVariantMap();
        if(newFlightData.isEmpty()){
            if(!flightData.isEmpty())
                emit stateChanged(ConnectedState);
        }
        else{
            if(flightData.isEmpty()){
                emit stateChanged(RunningState);
            }
            emit flightDataUpdated(&flightData);
        }
        flightData.swap(newFlightData);
    }
}
