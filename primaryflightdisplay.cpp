#include "primaryflightdisplay.h"

#include <qmath.h>
#include <Qpainter>

PrimaryFlightDisplay::PrimaryFlightDisplay(QWidget *parent) :
    QOpenGLWidget(parent),
    navball(0),
    overlay(0)
{
    attitude.heading=0.0;
    attitude.pitch=0.0;
    attitude.roll=0.0;
    setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding);
}

PrimaryFlightDisplay::~PrimaryFlightDisplay(){
    // Make sure the context is current when deleting the texture
    // and the buffers.
    makeCurrent();
    delete navball;
    delete overlay;
    delete attitudeText;
    doneCurrent();
}

void PrimaryFlightDisplay::initializeGL()
{
    initializeOpenGLFunctions();

    glClearColor(0, 0, 0, 1);

    initShaders();

    navball = new RenderedNavBall(240.0f,50,":/textures/NavBall.png");
    navball->setPosition(0.0f,0.0f,400.0f);

    overlay = new RenderedObject(800.0f,800.0f,":/textures/Overlay.png");
    overlay->setPosition(0.0f,0.0f,1.0f);

    QFont font = QFont("BaseOne");
    font.setPointSize(20);
    attitudeText = new RenderedText(6,3,40.0f,font);
    attitudeText->setCharSizeRatio(0.60f,0.75f);
    attitudeText->setPosition(265.0f,268.0f,1.0f);
}

void PrimaryFlightDisplay::initShaders()
{
    if (!program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/vshader"))
        close();
    if (!program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/fshader"))
        close();
    if (!program.link())
        close();
}

void PrimaryFlightDisplay::resizeGL(int w, int h)
{
    // Calculate aspect ratio
    //float aspect = float(w) / float(h ? h : 1);
    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    //const qreal zNear = 3.0, zFar = 7.0, fov = 45.0;
    // Reset projection
    //projection.setToIdentity();
    // Set perspective projection
    //projection.perspective(fov, aspect, zNear, zFar);

    float width = 800.0f;
    float height = 800.0f;

    width/=2;
    height/=2;
    if(w>h){
        float ratio = (float)w/(float)(h>0 ? h : 1);
        width*=ratio;
    }
    else if(h>w){
        float ratio = (float)h/(float)(w>0 ? w  : 1);
        height*=ratio;
    }

    projection.setToIdentity();
    projection.ortho(-width,width,-height,height, 0.0f,800.0f);
}

void PrimaryFlightDisplay::paintGL()
{
    navball->setAttitude(attitude.heading,attitude.pitch,attitude.roll);
    QString text ("H:%1%2\nP:%3%4\nR:%5%6");
    attitudeText->setText(text.arg(
                              attitude.heading<0 ? QChar('-') : QChar(' ')).arg(
                              qAbs(attitude.heading),3,'f',0,QChar('0')).arg(
                              attitude.pitch<0 ? QChar('-') : QChar(' ')).arg(
                              qAbs(attitude.pitch),3,'f',0,QChar('0')).arg(
                              attitude.roll<0 ? QChar('-') : QChar(' ')).arg(
                              qAbs(attitude.roll),3,'f',0,QChar('0')));

    glEnable(GL_CULL_FACE);
    //glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_MULTISAMPLE);

    // Clear color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    program.bind();
    program.setUniformValue("p_matrix", projection);
    program.setUniformValue("color_mask",QVector4D(1.0f,1.0f,1.0f,1.0f));

    navball->render(&program);
    overlay->render(&program);
    attitudeText->render(&program);

    program.release();

    //glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
}

void PrimaryFlightDisplay::updateFlightData(QVariantMap* flightData){
    if(flightData->contains("n.heading")) attitude.heading = qBound(0.0f,flightData->value("n.heading").toFloat(),360.0f);
    if(flightData->contains("n.pitch")) attitude.pitch = qBound(-90.0f,flightData->value("n.pitch").toFloat(),90.0f);
    if(flightData->contains("n.roll")) attitude.roll = qBound(-180.0f,flightData->value("n.roll").toFloat(),180.0f);
    update();
}
