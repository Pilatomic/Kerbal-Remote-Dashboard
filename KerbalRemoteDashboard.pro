#-------------------------------------------------
#
# Project created by QtCreator 2015-06-11T09:16:57
#
#-------------------------------------------------

QT       += core gui network websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KerbalRemoteDashboard
TEMPLATE = app


SOURCES += main.cpp\
        mainwidget.cpp \
    primaryflightdisplay.cpp \
    renderedobjects.cpp \
    datasource.cpp \
    settingsdialog.cpp

HEADERS  += mainwidget.h \
    primaryflightdisplay.h \
    renderedobjects.h \
    datasource.h \
    settingsdialog.h

FORMS    += mainwidget.ui \
    settingsdialog.ui

RESOURCES += \
    resources.qrc
