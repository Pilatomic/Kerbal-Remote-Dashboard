#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:

    struct Settings{
        int refreshRate;
        QString host;
        int port;
    };

    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();


private slots:
    void onOkButtonPressed();
    void onCancelButtonPressed();

signals:
    void newSettingsAccepted(Settings newSettings);

private:
    Ui::SettingsDialog *ui;

    Settings settings;

    void saveSettings();
};

#endif // SETTINGSDIALOG_H
