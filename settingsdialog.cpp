#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include <QDebug>

class IpValidator : public QValidator
{
public:
    IpValidator(QObject * parent = 0):QValidator(parent){

    }
    QValidator::State validate(QString & input, int & pos) const {
        Q_UNUSED(pos);
        QStringList byteList = input.split(QChar('.'));

        if(byteList.size() != 4){
            return QValidator::Invalid;
        }

        foreach(QString byteString, byteList){
            if(byteString == "   "){
                return QValidator::Intermediate;
            }
            bool ok;
            int byteInt = byteString.toInt(&ok);
            if(!ok || byteInt > 255 || byteInt < 0){
                return QValidator::Invalid;
            }
        }
        return QValidator::Acceptable;
    }
};


SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    ui->ipLineEdit->setValidator( new IpValidator(ui->ipLineEdit));

    saveSettings();

    connect(ui->okButton,&QPushButton::clicked,this,&SettingsDialog::onOkButtonPressed);
    connect(ui->cancelButton,&QPushButton::clicked,this,&SettingsDialog::onCancelButtonPressed);
}

void SettingsDialog::onOkButtonPressed(){
    saveSettings();
    emit newSettingsAccepted(settings);
    accept();
}

void SettingsDialog::onCancelButtonPressed(){
    ui->ipLineEdit->setText(settings.host);
    ui->rateSpinBox->setValue(settings.refreshRate);
    reject();
}

void SettingsDialog::saveSettings(){
    settings.host=ui->ipLineEdit->text();
    settings.refreshRate=ui->rateSpinBox->value();
    settings.port=ui->portSpinBox->value();
}


SettingsDialog::~SettingsDialog()
{
    delete ui;
}
