#ifndef WEBSOCKETINTERFACE_H
#define WEBSOCKETINTERFACE_H

#include <QObject>
#include <QtWebSockets/QWebSocket>
#include <QUrl>
#include <QList>
#include <QStringList>
#include <QMap>
#include <QVariant>

class DataSource : public QObject
{
    Q_OBJECT
public:
    enum State{NoConnectionState, ConnectingState, ConnectedState, RunningState};

    explicit DataSource(QObject *parent = 0);
    void registerDataSources(QStringList sourceList);
    void registerDataSource(QString source);

signals:
    void flightDataUpdated(QVariantMap* flightData);
    void stateChanged(State state);

public slots:
    void open();
    void close();
    void setServer(QString server, int port = 8085);
    void setDataRate(int newRate);

private slots:
    void onSocketStateChanged(QAbstractSocket::SocketState state);
    void onTextMessageReceived(QString message);

private:
    void sendRegisterMessage(QStringList sourceList);
    void sendRateMessage();

    QWebSocket webSocket;
    QUrl url;

    QStringList registeredData;
    int dataRate;

    QVariantMap flightData;
};

#endif // WEBSOCKETINTERFACE_H
