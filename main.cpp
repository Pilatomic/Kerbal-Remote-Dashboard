#include "mainwidget.h"
#include <QApplication>
#include <QSurfaceFormat>
#include <QFile>
#include <QFontDatabase>
#include <QMessageBox>

void loadEmbeddedFonts(QStringList *fontList){
    int fontID(-1);
    bool fontWarningShown(false);
    for (QStringList::const_iterator constIterator = fontList->constBegin(); constIterator != fontList->constEnd(); ++constIterator) {
        QFile res(":/fonts/" + *constIterator);
        if (res.open(QIODevice::ReadOnly) == false) {
            if (fontWarningShown == false) {
                QMessageBox::warning(0, "Application", (QString)"Impossible d'ouvrir la police " + QChar(0x00AB) + " DejaVu Serif " + QChar(0x00BB) + ".");
                fontWarningShown = true;
            }
        } else {
            fontID = QFontDatabase::addApplicationFontFromData(res.readAll());
            if (fontID == -1 && fontWarningShown == false) {
                QMessageBox::warning(0, "Application", (QString)"Impossible d'ouvrir la police " + QChar(0x00AB) + " DejaVu Serif " + QChar(0x00BB) + ".");
                fontWarningShown = true;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("Kerbal Instrument Panel");
    app.setApplicationVersion("0.01");

    QStringList fontList;
    fontList << "BaseOne.otf";
    loadEmbeddedFonts(&fontList);

    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    format.setSamples(4);
    QSurfaceFormat::setDefaultFormat(format);

    MainWidget widget;
    widget.show();

    return app.exec();
}
