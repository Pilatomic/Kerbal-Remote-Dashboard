#ifndef NAVBALL_H
#define NAVBALL_H

#include "renderedobjects.h"

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QBasicTimer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QMap>
#include <QVariant>

class PrimaryFlightDisplay : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    explicit PrimaryFlightDisplay(QWidget *parent = 0);
    ~PrimaryFlightDisplay();

    void initializeGL() Q_DECL_OVERRIDE;
    void resizeGL(int w, int h) Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;

    void initShaders();

public slots:

    void updateFlightData(QVariantMap* flightData);

private:

    QOpenGLShaderProgram program;
    RenderedNavBall* navball;
    RenderedObject* overlay;
    RenderedText* attitudeText;

    QMatrix4x4 projection;
    struct{
        float heading;
        float pitch;
        float roll;
    }attitude;
};

#endif // NAVBALL_H
