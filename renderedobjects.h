#ifndef SPHERE_H
#define SPHERE_H

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QVector3D>
#include <QFont>



class RenderedObject : protected QOpenGLFunctions
{
public:
    RenderedObject();
    RenderedObject(qreal width, qreal height, QString filename);
    virtual ~RenderedObject();

    virtual void render(QOpenGLShaderProgram *program);
    void setPosition(qreal x,qreal y, qreal z);

protected:
    void initOpenGLStuff();
    virtual void initTexture(QString filename);
    virtual void initGeometry(qreal width, qreal height);
    virtual void doRendering(QOpenGLShaderProgram *program, QMatrix4x4 mvMatrix);

    QOpenGLTexture *texture;
    QOpenGLBuffer vertexBuf;
    QOpenGLBuffer indexBuf;
    uint indexCount;
    QVector3D position;

    struct VertexData
    {
        QVector3D position;
        QVector2D texCoord;
    };
};



class RenderedNavBall : public RenderedObject
{
public:
    RenderedNavBall(float radius, int precision, QString Texture);
    void render(QOpenGLShaderProgram *program);
    void setAttitude(qreal heading, qreal pitch, qreal roll);

private:
    void initGeometry(float radius, int precision);
    VertexData generateVertexFromPolarCoord(float r, float latitudeRatio, float longitudeRatio);

    struct{
        qreal heading;
        qreal pitch;
        qreal roll;
    } attitude;
};

class RenderedText : public RenderedObject{
public:
    RenderedText(int charPerLine, int lines, qreal charSize, QFont font = QFont());
    void setCharSizeRatio(float charWidthPercent = 1.0f, float charHeightPercent = 1.0f);
    void setText(QString text);

protected:
    void initGeometry();
    void initTexture(QFont font);
    VertexData createVertice(int x, int y);
    void assignCharToQuad(int quad, char ch);

    int supportedCharCount;
    int charPerLine;
    int lines;
    float charSize;

    float charAspectRatio;
    float overrideCharWidth;
    float overrideCharHeight;

    QVector<VertexData> verticesVector;
};

#endif // SPHERE_H
