#include "renderedobjects.h"

#include <qmath.h>
#include <QFontMetrics>
#include <QPainter>

RenderedObject::RenderedObject(){
    initOpenGLStuff();
}


RenderedObject::RenderedObject(qreal width, qreal height, QString filename){
    initOpenGLStuff();
    initGeometry(width, height);
    initTexture(filename);
}

void RenderedObject::initOpenGLStuff(){
    indexBuf = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
    vertexBuf = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);

    initializeOpenGLFunctions();

    // Generate 2 VBOs
    vertexBuf.create();
    indexBuf.create();
}


void RenderedObject::render(QOpenGLShaderProgram *program){
    QMatrix4x4 matrix;
    matrix.translate(position);
    doRendering(program, matrix);
}



void RenderedObject::setPosition(qreal x, qreal y, qreal z){
    position.setX(x);
    position.setY(y);
    position.setZ(-z);
}


void RenderedObject::initTexture(QString filename){
    // Load NavBall.png image. If not found, display a red sphere
    QImage image = QImage(filename).mirrored();
    if(image.width() == 0 || image.height() == 0){
        qWarning()<<"Texture "<<filename<<" missing, using plain red";
        image = QImage(100,100,QImage::Format_RGB32);
        image.fill(Qt::red);
    }
    texture = new QOpenGLTexture(image);

    // Set nearest filtering mode for texture minification
    texture->setMinificationFilter(QOpenGLTexture::Linear);

    // Set bilinear filtering mode for texture magnification
    texture->setMagnificationFilter(QOpenGLTexture::Linear);

    // Wrap texture coordinates by repeating
    // f.ex. texture coordinate (1.1, 1.2) is same as (0.1, 0.2)
    texture->setWrapMode(QOpenGLTexture::Repeat);
}

void RenderedObject::initGeometry(qreal width, qreal height){

    width = (width > 0) ? width/2 : 1.0f;
    height = (height > 0) ? height/2 : 1.0;

    VertexData vertices[] = {
        {QVector3D(-width, -height,  0.0f), QVector2D(0.0f, 0.0f)},
        {QVector3D( width, -height,  0.0f), QVector2D(1.0f, 0.0f)},
        {QVector3D(-width,  height,  0.0f), QVector2D(0.0f, 1.0f)},
        {QVector3D( width,  height,  0.0f), QVector2D(1.0f, 1.0f)}};

    GLushort indices[] = {0,  1,  2,  3, 3};
    // Transfer vertex data to VBO 0
    vertexBuf.bind();
    vertexBuf.allocate(vertices, 4 * sizeof(VertexData));
    vertexBuf.release();

    // Transfer index data to VBO 1
    indexBuf.bind();
    indexBuf.allocate(indices, 5 * sizeof(GLushort));
    indexBuf.release();

    indexCount = 5;
}

void RenderedObject::doRendering(QOpenGLShaderProgram *program, QMatrix4x4 mvMatrix){
    //Bing texture
    texture->bind();

    // Set modelview matrix
    program->setUniformValue("mv_matrix", mvMatrix);

    // Use texture unit 0 which contains cube.png
    program->setUniformValue("texture", 0);

    // Tell OpenGL which VBOs to use
    vertexBuf.bind();
    indexBuf.bind();

    // Offset for position
    quintptr offset = 0;

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertexLocation = program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocation);
    program->setAttributeBuffer(vertexLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocation = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocation);
    program->setAttributeBuffer(texcoordLocation, GL_FLOAT, offset, 2, sizeof(VertexData));

    // Draw cube geometry using indices from VBO 1
    glDrawElements(GL_TRIANGLE_STRIP, indexCount, GL_UNSIGNED_SHORT, 0);

    vertexBuf.release();
    indexBuf.release();
    texture->release();
}

RenderedObject::~RenderedObject(){
    vertexBuf.destroy();
    indexBuf.destroy();
    delete texture;
}


RenderedNavBall::RenderedNavBall(float radius, int precision, QString Texture){
    initGeometry(radius,precision);
    initTexture(Texture);
}


RenderedObject::VertexData RenderedNavBall::generateVertexFromPolarCoord(float r, float latitudeRatio, float longitudeRatio){
    const float Pi = 3.14159f;
    VertexData myVertice;
    myVertice.position=QVector3D(-r* qSin(latitudeRatio*Pi)*qSin(longitudeRatio*2.0f*Pi),
                                 r* qCos(latitudeRatio*Pi),
                                 -r* qSin(latitudeRatio*Pi)*qCos(longitudeRatio*2.0f*Pi));

    myVertice.texCoord=QVector2D(longitudeRatio,
                                 1.0f-latitudeRatio);
    return myVertice;
}

void RenderedNavBall::initGeometry(float radius, int precision)
{
    const float r = radius;

    if(radius<=0)radius = 1;
    int latitudeVertexCount = qMax(precision,3);
    int longitudeVertexCount = qMax(precision,3);

    QVector<VertexData> verticesVector = QVector<VertexData>();
    QVector<GLushort>   indexVector    = QVector<GLushort>();

    //Fills vertices vector
    for(int i = 0;i<latitudeVertexCount;i++){
        for(int j = 0; j<=longitudeVertexCount;j++){
            verticesVector.append(generateVertexFromPolarCoord(r,
                                                               (float)i/(float)(latitudeVertexCount-1),
                                                               (float)j/(float)longitudeVertexCount));
        }
    }

    //Fills indices vector
    for(int i = 0;i<latitudeVertexCount;i++){
        for(int j = 0; j<=longitudeVertexCount;j++){
            if(i > 0 || j > 0) indexVector.append((i+1)*longitudeVertexCount+j);
            indexVector.append((i+1)*longitudeVertexCount+j);
            indexVector.append((i+1)*longitudeVertexCount+((j+1)%(longitudeVertexCount+1)));
            indexVector.append(i*longitudeVertexCount+j);
            indexVector.append(i*longitudeVertexCount+((j+1)%(longitudeVertexCount+1)));
            indexVector.append(i*longitudeVertexCount+((j+1)%(longitudeVertexCount+1)));
        }
    }
    indexCount = indexVector.count();

    // Transfer vertex data to VBO 0
    vertexBuf.bind();
    vertexBuf.allocate(verticesVector.data(), verticesVector.count() * sizeof(VertexData));
    vertexBuf.release();

    // Transfer index data to VBO 1
    indexBuf.bind();
    indexBuf.allocate(indexVector.data(), indexCount * sizeof(GLushort));
    indexBuf.release();
}


void RenderedNavBall::render(QOpenGLShaderProgram *program)
{
    QMatrix4x4 matrix;

    matrix.translate(position);
    matrix.rotate(attitude.roll,0,0,-1);
    matrix.rotate(attitude.pitch,1,0,0);
    matrix.rotate(attitude.heading,0,-1,0);

    doRendering(program,matrix);
}

void RenderedNavBall::setAttitude(qreal heading, qreal pitch, qreal roll){
    attitude.heading=heading;
    attitude.pitch=pitch;
    attitude.roll=roll;
}

RenderedText::RenderedText(int charPerLine, int lines, qreal charSize, QFont font):
     supportedCharCount(128), charPerLine(0), lines(0), charAspectRatio(1.0f),overrideCharWidth(1.0f), overrideCharHeight(1.0f)
{
    this->charPerLine = qMax(charPerLine, 1);
    this->lines = qMax(lines,1);
    this->charSize = charSize;

    initTexture(font);
    initGeometry();
}

void RenderedText::setCharSizeRatio(float charWidthPercent, float charHeightPercent){
    overrideCharWidth = qBound(0.1f,charWidthPercent,10.0f);
    overrideCharHeight = qBound(0.1f,charHeightPercent,10.0f);

    initGeometry();
}

void RenderedText::initTexture(QFont font){
    QFontMetrics metrics(font);
    int charHeight = metrics.height()+metrics.leading();
    int charWidth=0;

    for(int i = 0 ; i < supportedCharCount ; i++){
        charWidth= qMax(charWidth,metrics.width(QChar(i)));
    }

    QImage image(charWidth*supportedCharCount,charHeight,QImage::Format_ARGB32);
    image.fill(0x00000000); //Transparent image

    QPainter painter(&image);
    painter.setFont(font);
    painter.setPen(Qt::white);

    for(int i = 0 ; i < supportedCharCount ; i++){
        painter.drawText(i*charWidth,charHeight,QString(QChar(i)));
    }

    painter.end();

    texture = new QOpenGLTexture(image);
    texture->setMinificationFilter(QOpenGLTexture::Linear);
    texture->setMagnificationFilter(QOpenGLTexture::Linear);
    texture->setWrapMode(QOpenGLTexture::ClampToEdge);


    charAspectRatio =  ((float)charWidth) / ((float)charHeight);
}

void RenderedText::initGeometry(){
    QVector<GLushort>   indexVector    = QVector<GLushort>();
    verticesVector.clear();

    for(int i = 0 ; i < lines ; i++){
        for(int j = 0 ; j < charPerLine ; j++){
            verticesVector.append(createVertice(j,i));
            verticesVector.append(createVertice(j,i+1));
            verticesVector.append(createVertice(j+1,i));
            verticesVector.append(createVertice(j+1,i+1));

            int charNum=(i*charPerLine+j)*4;

            if(charNum != 0)
                indexVector.append(charNum);

            indexVector.append(charNum);
            indexVector.append(charNum+1);
            indexVector.append(charNum+2);
            indexVector.append(charNum+3);
            indexVector.append(charNum+3);
        }
    }

    indexCount = indexVector.count();

    // Transfer vertex data to VBO 0
    vertexBuf.bind();
    vertexBuf.allocate(verticesVector.data(), verticesVector.count() * sizeof(VertexData));
    vertexBuf.release();

    // Transfer index data to VBO 1
    indexBuf.bind();
    indexBuf.allocate(indexVector.data(), indexCount * sizeof(GLushort));
    indexBuf.release();
}

RenderedObject::VertexData RenderedText::createVertice(int x, int y){
    VertexData vertice;
    vertice.position=QVector3D(((float)x - (float)charPerLine/2 )* charSize * overrideCharWidth,
                               ((float)lines / 2 -(float)y) * (charSize/charAspectRatio) * overrideCharHeight,
                               0.0f);
    vertice.texCoord=QVector2D(0.0f,0.0f);
    return vertice;
}

void RenderedText::setText(QString text){

    QByteArray textU8 = text.toUtf8();

    int quadIndex = 0;
    int charIndex = 0;

    while(quadIndex < lines*charPerLine && charIndex < textU8.size()){
        char currentChar = textU8.at(charIndex);
        if(currentChar < supportedCharCount){
            if(currentChar == '\n'){
                while(quadIndex % charPerLine != 0){
                    assignCharToQuad(quadIndex,' ');
                    quadIndex++;
                }
            }
            else{
                assignCharToQuad(quadIndex,currentChar);
                quadIndex++;
            }
        }
        charIndex++;
    }

    //Fill left quad with spaces
    while( quadIndex  < lines*charPerLine){
        assignCharToQuad(quadIndex,' ');
        quadIndex++;
    }

    //Update vertex buffer
    vertexBuf.bind();
    vertexBuf.allocate(verticesVector.data(), verticesVector.count() * sizeof(VertexData));
    vertexBuf.release();
}

void RenderedText::assignCharToQuad(int quad, char ch){
    int baseAddress = quad*4;
    if(baseAddress>verticesVector.count()){
        qWarning()<<"Attempted to set character out of bounds";
        return;}

    float charPos = (float)ch/(float)supportedCharCount;
    float nextCharPos = charPos + overrideCharWidth/(float)supportedCharCount;

    verticesVector[baseAddress].texCoord=QVector2D(charPos,1.0f-overrideCharHeight);
    verticesVector[baseAddress+1].texCoord=QVector2D(charPos,1.0f);
    verticesVector[baseAddress+2].texCoord=QVector2D(nextCharPos,1.0f-overrideCharHeight);
    verticesVector[baseAddress+3].texCoord=QVector2D(nextCharPos,1.0f);
}

